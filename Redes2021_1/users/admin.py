from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

# Register your models here.
from .forms import SignUpForm, AccountModifyForm
from .models import Account, Score

class AccountAdmin(UserAdmin):
    add_form = SignUpForm
    form = AccountModifyForm
    model = Account
    
    fieldsets = (
        (None, {'fields': ('nickname', 'tipo')}),
        ('Datos', {'fields': ('correo',"password")}),
    )
    # add_fieldsets is not a standard ModelAdmin attribute. UserAdmin
    # overrides get_fieldsets to use this attribute when creating a user.
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('nickname', 'password1', 'password2', "tipo"),
        }),
    )    
    
    list_display = ['nickname', 'correo']
    list_filter = ['tipo']
    ordering = ["nickname", 'correo']

admin.site.register(Account, AccountAdmin)
admin.site.register(Score)
