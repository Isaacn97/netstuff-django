# Django

from django import forms
from django.forms import ModelForm, Form
from django.core.exceptions import ValidationError

# Auth
from django.contrib.auth.models import User
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django.contrib.auth import authenticate

# Modelos
from users.models import *

class SignUpForm(UserCreationForm):
    """Define un formulario para crear Usuario"""
    
    def clean_username(self):
        # Since Account.nickname is unique, this check is redundant,
        # but it sets a nicer error message than the ORM. See #13147.
        nickname = self.cleaned_data["nickname"]
        try:
            Account._default_manager.get(nickname=nickname)
        except Account.DoesNotExist:
            return nickname
        raise forms.ValidationError(self.error_messages['Ya existe una cuenta con este nickname'])
    
    class Meta(UserCreationForm):
        model = Account
        fields = ['nickname']
        labels = {
            'nickname': ('Nombre de cuenta'),
        }

class AccountModifyForm(UserChangeForm):
    """Define un formulario para modificar una cuenta"""
    class Meta:
        model = Account
        fields = ['nickname']
        labels = {
            'nickname': ('Nombre de cuenta')
        }

class AccountLoginForm(Form):
    """Define un formulario para iniciar sesión"""
    username = forms.CharField()
    password = forms.CharField()
    #password = forms.PasswordInput()
    labels = {
        'username': ('Nick'),
        'password': ('Contraseña'),
    }
