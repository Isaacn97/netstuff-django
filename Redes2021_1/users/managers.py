from django.contrib.auth.base_user import BaseUserManager
from django.utils.translation import ugettext_lazy as _

class AccountManager(BaseUserManager):
    """
    Custom user model manager in which a nickname (nick) is the unique
    identifier for authentication.
    """
    def create_user(self, nickname, password, **extra_fields):
        """
        Create and save a User with the given nick and password.
        """
        extra_fields.setdefault('tipo', 2)
        
        if not nickname:
            raise ValueError(_('Debes tener un nickname'))
        user = self.model(nickname=nickname, **extra_fields)
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, nickname, password, **extra_fields):
        """
        Create and save a SuperUser with the given nickname and password.
        """
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('tipo', 1)
        
        if extra_fields.get('is_superuser') is not True:
            raise ValueError(_('Superuser must have is_superuser=True.'))
        extra_fields.setdefault('tipo', 2)
        
        if not nickname:
            raise ValueError(_('Debes tener un nickname'))
        user = self.model(nickname=nickname, **extra_fields)
        user.set_password(password)
        user.save()
        return user
