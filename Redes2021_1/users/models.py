# Django
from django.db import models
from django.core.validators import EmailValidator
from django.core.exceptions import NON_FIELD_ERRORS
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _

# Auth
from django.contrib.auth.models import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from .managers import AccountManager

def numero_telefono(value):
    """Verifica que un charfield tenga una longitud adecuada y que sólo contenga números"""
    if len(value) < 8:
        raise ValidationError(
            _('%(value)s Es demasiado corto'),
            code='min_length',
            params={'value': value},
            )
    for e in value:
        if(e >= '0' and e <= '9'):
            return
        else:
            raise ValidationError(
                _('%(value)s Sólo debe contener números'),
                code='invalid',
                params={'value': value},
                )
        
class Account(AbstractBaseUser, PermissionsMixin):
    """Modelo para la BD de una cuenta"""
    nickname = models.CharField(max_length = 80, unique=True,
                              error_messages=
                              {"required":"Necesitas escribir un nick",
                                'unique':"Este nick ya esta en uso",
                               "max_length":"Tu nick debe teer 80 caracteres máximo"})
    correo = models.EmailField(blank=True, null=True, max_length = 120)
    #Nota: el atributo ID de la entidad existe por defecto en Django
    
    # Perrmisos y auth
    TIPOS_DE_USUARIO = ( 
        (1, "Admin"),
        (2, "User")
    )
    tipo = models.PositiveSmallIntegerField(choices=TIPOS_DE_USUARIO)
    is_staff = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)
    
    USERNAME_FIELD = 'nickname'
    REQUIRED_FIELDS = []
    objects = AccountManager()    
    
    def __str__(self):
        """Obtener represencacion como cadena"""
        return f"{self.nickname}"

    def __repr__(self):
        """Obtener represencacion como cadena"""
        return self.__str__()
    

class Score(models.Model):
    "Clase para representar puntuaciones de un usuario"
    score = models.IntegerField(null=False)
    
    # Relaciones de entidad
    owner = models.ForeignKey('Account',
        on_delete=models.CASCADE,)
    
    def __str__(self):
        """Obtener represencacion como cadena"""
        #dirs = list(self.all())
        return f"{self.score}pts by {self.owner}"
    
    def __repr__(self):
        """Obtener represencacion como cadena"""
        return self.__str__()
