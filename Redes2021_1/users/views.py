from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.views import View
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.contrib.auth import authenticate, login, logout
from django.urls import reverse_lazy
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView
from django.contrib import messages

#Models
from django.db import models
from .models import Score
from .models import Account

# Forms
from .forms import SignUpForm
    

class CreateAccount(View):
    """Página para crear una cuenta"""
    
    template = "users/signUp.html"
    
    def get(self, request):
        form = SignUpForm()
        context = {'form': form}
        return render(request, self.template, context)
        
    def post(self, request):
        form = SignUpForm(request.POST)
        
        if form.is_valid():
            
            form.instance.tipo ='2'
            form.save()
            
            return render( request, 'users/success.html', {'form': form})
        else:
            template = self.template
            return render( request, self.template, {'form': form})


@method_decorator(login_required, name='dispatch')    
class OwnScores(View):
    """Página para ver las puntuaciones asociadas al usuario autenticado"""
    
    template = "users/score.html"
    
    def get(self, request, pk):
        
        if request.user != None and request.user.id == pk:            
            """GET method."""
            puntajes = Score.objects.filter(owner=request.user).all()
            context = {"puntajes": puntajes}
            return render(request, self.template, context)
        else:
            return render(request, "users/err.html")
