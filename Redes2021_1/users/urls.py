# Django
from django.contrib import admin
from django.urls import include, path
from django.conf import settings
from django.conf.urls.static import static

# Views
from users import views

app_name = "users"

urlpatterns = [
    #Views de creación de cuentas 
    path("sign-up", views.CreateAccount.as_view(), name='create-account'),
    #Views de puntuaciones
    path('score/<int:pk>', views.OwnScores.as_view(), name='self-score'),
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
