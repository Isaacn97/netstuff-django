"""Redes2021_1 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path

from Redes2021_1 import views

urlpatterns = [
    #Views de acciones
    path('', views.RedirectHome.as_view(), name='redirectHome'),
    path('home', views.Home.as_view(), name='home'),
    path('out', views.Logout.as_view(), name='logout'),
    #Django
    path('admin/', admin.site.urls),
    #Stuff
    path('u/', include('users.urls')),
    path('g/', include('game.urls')),
]
