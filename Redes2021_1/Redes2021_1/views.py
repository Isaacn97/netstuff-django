from django.shortcuts import render, redirect
from django.views import View
from django.http import HttpResponse
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages

# Forms
from users.forms import AccountLoginForm

class Home(View):
    """Página de home e inicio de sesion"""
    
    template = "home.html"

    def get(self, request):
        form = AccountLoginForm()
        context = {"form": form}
        return render(request, self.template, context)

    def post(self, request):
        form = AccountLoginForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            user = authenticate(nickname=cd.get('username'), password=cd.get('password') )
            if user is not None:
                login(request, user)
                return redirect("/")
            else:
                messages.info(request, 
                              'Los datos no son correctos. Intenta de nuevo')
                return render(request, self.template, {'form': form,
                                                "contrib_messages":messages})
        else:
            messages.info(request, 'Asegurate de llenar los campos como se pide')
            return render(request, self.template, {'form': form,
                                                "contrib_messages":messages})

class Logout(View):
    """Página para cerrar sesion"""
    
    def get(self, request):
        logout(request)
        return redirect('/')
    
class RedirectHome(View):
    """Redireccion a Home"""
    
    def get(self, request):
        return redirect('/home')
