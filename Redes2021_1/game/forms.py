# Django

from django import forms
from django.forms import ModelForm, Form
from django.core.exceptions import ValidationError

# Modelos
from users.models import Score

class SaveScoreForm(ModelForm):
    """Define un formulario para crear Usuario"""
    
    class Meta():
        model = Score
        exclude = ("owner",)
        fields = ['score']
        labels = {
            'score': ('Score to be saved'),
        }
