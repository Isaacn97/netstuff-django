from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.views import View
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.contrib.auth import authenticate, login, logout
from django.urls import reverse_lazy
from django.views.generic import TemplateView
from django.contrib import messages

#Models
from django.db import models
from users.models import Score
from users.models import Account

#Forms
from game.forms import SaveScoreForm
    

class Dinosaur(View):
    """Página para jugar el juego del dinosaurio de google"""
    
    template = "game/dino.html"
    
    def get(self, request):
        form = SaveScoreForm()
        context = {'form': form}
        return render(request, self.template, context)
        
    def post(self, request):
        form = SaveScoreForm(request.POST)
        if request.user != None:
            
            if form.is_valid():
                form.instance.owner=Account.objects.get(id=request.user.id)
                form.save()
                
                return render( request, 'game/success.html')
            else:               
                return render( request, 'game/fail.html', {'form': form})
        else:
            return render(request, self.template, context)
