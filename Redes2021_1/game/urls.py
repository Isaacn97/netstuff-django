# Django
from django.contrib import admin
from django.urls import include, path
from django.conf import settings
from django.conf.urls.static import static

# Views
from game import views

app_name = "game"

urlpatterns = [
    #Views de juegos
    path('', views.Dinosaur.as_view(), name='dinosaur1'),
    path('dino', views.Dinosaur.as_view(), name='dinosaur'),
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
